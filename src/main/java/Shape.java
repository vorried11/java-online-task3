public class Shape{
    private String colour;

    public Shape (String colour){
        this.colour = colour;
    }

    public Shape() {

    }

    public String getColour(){
        return colour;
    }
}