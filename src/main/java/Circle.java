public class Circle extends Shape {
    public Circle(String colour){
        super(colour);
    }

    public void printColour(){
        System.out.println(getColour());
    }
}
