public class Triangle extends Shape {
    public Triangle(String colour){
        super(colour);
    }

    public void printColour(){
        System.out.println(getColour());
    }
}
