public class Rectangle extends Shape {
    public Rectangle(String colour){
        super(colour);
    }

    public void printColour(){
        System.out.println(getColour());
    }
}
