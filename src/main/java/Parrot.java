public class Parrot extends Bird {
    @Override
    public void eat (){
        System.out.println("Parrot is eating some food");
    }
    @Override
    public void sleep(){
        System.out.println("Parrot is sleeping");
    }
    @Override
    public void fly(){
        System.out.println("Parrot is flying");
    }
}
