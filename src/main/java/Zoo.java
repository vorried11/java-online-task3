public class Zoo {
    public static void main(String args[]){
        Parrot parr =  new Parrot();
        Eagle eagl = new Eagle();
        Bird brb = new Bird();
        Fish fish1 = new Fish();

        brb.eat();
        fish1.eat();
        eagl.eat();
        parr.eat();
    }



}
