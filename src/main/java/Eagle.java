public class Eagle extends Bird {
    @Override
    public void eat (){
        System.out.println("Eagle is eating meat");
    }
    @Override
    public void sleep(){
        System.out.println("Eagle is sleeping");
    }
    @Override
    public void fly(){
        System.out.println("Eagle is flying");
    }

}
